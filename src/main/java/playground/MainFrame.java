package playground;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame
{
    private final JTabbedPane tabsPane;

    public MainFrame()
    {
        setTitle("PRO1");
        setVisible(true);
        setBackground(Color.white);
        setSize(600, 800);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        tabsPane = new JTabbedPane();
        add(tabsPane);
        JPanel drawingPanel = new DrawingPanel();
        drawingPanel.setBackground(Color.white);
        tabsPane.addTab("Kreslení", drawingPanel);
        tabsPane.addTab("Světové strany", new BorderLayoutPanel());
        tabsPane.addTab("Časová řada", new TimeSeriesPanel());
    }
}
