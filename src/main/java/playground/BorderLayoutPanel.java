package playground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class BorderLayoutPanel extends JPanel
{
    public BorderLayoutPanel()
    {
        setLayout(new BorderLayout());
        JButton nahoreButton = new JButton("Nahoře");
        nahoreButton.setBackground(Color.blue);
        add(nahoreButton, BorderLayout.NORTH);
        add(new JButton("Dole"), BorderLayout.SOUTH);
        add(new JButton("Vlevo"), BorderLayout.WEST);
        add(new JButton("Vpravo"), BorderLayout.EAST);




    }
}
