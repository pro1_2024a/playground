package playground;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

public class TimeSeriesPanel extends JPanel
{
    public TimeSeriesPanel()
    {
        JToolBar toolBar = new JToolBar();
        JTextField countTextField = new JTextField();
        JButton confirmButton = new JButton("Nastavit");
        JButton exportButton = new JButton("Export");
        JPanel flowPanel = new JPanel();
        flowPanel.setLayout(new FlowLayout());

        toolBar.add(countTextField);
        toolBar.add(confirmButton);
        toolBar.add(exportButton);

        setLayout(new BorderLayout());
        add(toolBar, BorderLayout.NORTH);
        add(flowPanel, BorderLayout.CENTER);

        List<TextField> textFields = new ArrayList<>();

        confirmButton.addActionListener((e)->{
            int count = Integer.parseInt(countTextField.getText());
            for (int i=0; i<count;i++)
            {
                TextField field = new TextField();
                flowPanel.add(field);
                textFields.add(field);
            }
        });

        exportButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(TextField f : textFields)
                {
                    System.out.println(f.getText());
                }
            }
        });



    }
}
